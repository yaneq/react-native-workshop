import React, {Component} from 'react';
import {
    View,
    Text,
    TouchableOpacity, 
    TextInput,
    StyleSheet
} from 'react-native';
import firebase from 'react-native-firebase';

export default class MainScreen extends Component {
    constructor() {
        super();
        this.clickstore = firebase.firestore().collection('clicks');
        this.unsubscribe = null;
        this.state = {
            name: "",
            clicks: []
        };
    }

    componentDidMount() {
        this.unsubscribe = this
            .clickstore
            .onSnapshot(this._onCollectionUpdate) 
    }

    componentWillUnmount() {
        this.unsubscribe();
    }

    _onCollectionUpdate = (querySnapshot) => {
        let clicks = [];
        querySnapshot.forEach((doc) => {
            clicks.push(doc.data());
        });
        this.setState({
            ...this.state,
            clicks
        })
    }

    _addClick = () => {
        this.clickstore.add({
            name: this.state.name,
            createdAt: new Date()
        });
        this.setState({
            ...this.state,
            name: ''
        })
    }

    render() {
        return(
            <View style={{width: 300}}>
                <View>
                    {
                        this.state.clicks.map((entry, index) => 
                            <Text 
                                style={styles.entry}
                                key={index}>{entry.name}
                            </Text>)
                    }
                </View>
                <View>
                    <TextInput
                        style={styles.nameInput}
                        onChangeText={(name) => this.setState({...this.state, name})}
                        value={this.state.name}
                    ></TextInput>
                    <TouchableOpacity
                        onPress={this._addClick}
                        style={styles.buttonContainer}
                    >
                        <Text style={styles.buttonText}>
                            ClickMe
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    entry: {
      padding: 10,
      margin: 3,
      borderWidth: 1,
      borderColor: 'black',
      fontWeight: '200'
    },
    nameInput: {
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        margin: 20
    },
    buttonContainer: {
        margin: 20,
        backgroundColor: "green",
        paddingVertical: 10
    },
    buttonText: {
        color: 'white',
        fontWeight: "500",
        fontSize: 20,
        textAlign: 'center'
    }
  });
